package com.example.mycalc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int a, c, res;
    TextView resultView, inputView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultView = findViewById(R.id.result_view);
        inputView = findViewById(R.id.input_view);
    }

    public void btn_key(View v) {
        Button temp = (Button) v;
        String s = (String) temp.getText();
        this.inputView.setText(this.inputView.getText() + s);
    }
    public void calculation(View v) {
        String x = (String)inputView.getText();
        int operation = 0;
        for(int i=0; i < x.length(); i++) {
            if (!isNumeric(String.valueOf(x.charAt(i)))) {
                operation = i;
            }
        }
        if (operation == 0 && operation == x.length()-1) {
            Toast.makeText(getApplicationContext(), "제대로 입력하세요!!", Toast.LENGTH_LONG).show();
            this.inputView.setText("");
            return;
        }
        double num_a = Integer.parseInt(x.substring(0, operation));
        double num_b = Integer.parseInt(x.substring(operation+1));
        double result = 0;
        switch(x.charAt(operation)){
            case '+':
                result = num_a + num_b;
                break;
            case '-':
                result = num_a - num_b;
                break;
            case '*':
                result = num_a * num_b;
                break;
            case '/':
                result = num_a / num_b;
                break;
        }
        this.resultView.setText(Double.toString(result));
        this.inputView.setText("");
    }

    public void btn_delete(View v) {
        String origin = (String)this.inputView.getText();
        if (origin.length() == 0) return;
        this.inputView.setText(origin.substring(0, origin.length()-1));
    }

    private static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}